#-*- coding:utf8 -*-

from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw 
import MySQLdb

def generarCard(colorZona, nombre, puesto , id):
  print("generando imagen " + str(id) + "...")
# vars
  posy = 190
  espacio = 16
  color= (48,50,53)
  margen = 52

  if  nombre2 == " ":
    linea = 1
  else:
    linea = 2


  img = Image.open(colorZona + ".jpg")
  draw = ImageDraw.Draw(img)
  font = ImageFont.truetype("Fonts/HelveticaLTStd-BoldCond.otf", 11)
  fontDatos = ImageFont.truetype("Fonts/HelveticaLTStd-Cond.otf", 11)
  draw.text((30, posy), nombre1,color,font=font)
  draw.text((30, posy+(espacio*1)), nombre2,color,font=font)

  draw.text((30, posy+(espacio*linea)), organizacion1,color,font=fontDatos)
  linea = linea + 1
  draw.text((30, posy+(espacio*linea)), organizacion2,color,font=fontDatos)
  if  organizacion2 != " ":
    linea = linea + 1

  draw.text((30, posy+(espacio*linea)), puesto1,color,font=fontDatos)
  linea = linea + 1
  draw.text((30, posy+(espacio*linea)), puesto2,color,font=fontDatos)
  linea = linea + 1
  draw.text((30, posy+(espacio*linea)), puesto3,color,font=fontDatos)
  #draw.text((30, posy+(espacio*2)), unicode("PRESIDENTE DE LA JUNTA DE GOBIERNO Y \nCOORDINACIÓN POLÍTICA DEL CONGRESO DEL ESTADO DE GUANAJUATO",'UTF-8'),color,font=font)
  linea = linea + 2
  draw.text((30, posy+(espacio*linea)), "FOLIO: " + str(id).rjust(5,"0"),color,font=fontDatos)
  linea = linea + 1
  draw.text((30, posy+(espacio*linea)), "Acceso por Vasco de Quiroga",color,font=fontDatos)
  img.save('images/' + linkid + ".jpg", quality=100)

if __name__ == '__main__':
  db = MySQLdb.connect(host="127.0.0.1",user="root",passwd="Polomar01",db="invitacionesdb")   
  cur = db.cursor()
  cur.execute("SELECT * FROM contactos")
  print("consultando database . . . ")
  for row in cur.fetchall():
    uniqueID = row[0]
    linkid = row[1]
    zona = row[2]
    nombre = row[4]
    organizacion = row[5]
    puesto = row[6]

    if len(nombre) > 50:
      pos = 50
      while pos > 0:
        if nombre[pos] == " ":
          break
        pos = pos - 1
      nombre1 = nombre[:pos]
      nombre2 = nombre[pos+1:100]
    else:
      nombre1 = nombre
      nombre2 = " "

    if len(nombre) < 1:
      nombre1 = " "
      nombre2 = " "

    if len(organizacion) > 50:
      pos = 50
      while pos > 0:
        if organizacion[pos] == " ":
          break
        pos = pos - 1
      organizacion1 = organizacion[:pos]
      organizacion2 = organizacion[pos+1:100]
    else:
      organizacion1 = organizacion
      organizacion2 = " "

    if len(organizacion) < 1:
      organizacion1 = " "
      organizacion2 = " "

    if len(puesto) > 100:
      pos = 50
      while pos > 0:
        if puesto[pos] == " ":
          break
        pos = pos - 1
      posp1 = pos
      
      pos = 100
      while pos > posp1:
        if puesto[pos] == " ":
          break
        pos = pos - 1

      puesto1 = puesto[:posp1]
      puesto2 = puesto[posp1+1:pos]
      puesto3 = puesto[pos+1:]
    else:  
      if len(puesto) > 50 and len(puesto) < 100:
        pos = 50
        while pos > 0:
          if puesto[pos] == " ":
            break
          pos = pos - 1
        puesto1 = puesto[:pos]
        puesto2 = puesto[pos+1:]
        puesto3 = " "
      else:
        puesto1 = puesto
        puesto2 = " "
        puesto3 = " "

    if len(puesto) < 1:
      puesto1 = " "
      puesto2 = " "
      puesto3 = " "        
    generarCard(zona,nombre, puesto, uniqueID)